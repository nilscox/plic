module.exports.image = {

  hd: {
    size: 1600,
    path: 'assets/images/hd'
  },

  web: {
    size: 640,
    path: 'assets/images/web'
  },

  thumb: {
    size: 120,
    path: 'assets/images/thumb'
  },


};
