import {Component, OnInit} from '@angular/core';
import {User} from '../models/user';
import {UserService} from '../services/user.service';

@Component({
  selector: 'navbar',
  templateUrl: 'client/layout/navbar.component.html',
  providers: [UserService]
})

export class NavbarComponent implements OnInit {

  isAuthenticated: boolean = false;
  username: string;
  notificationCount: number = 0;

  constructor(
    private userService: UserService
  ) {
    UserService.onUserChange.subscribe(this.setUser.bind(this));
    UserService.onNotificationSeen.subscribe(() => this.notificationCount = 0);
  }

  ngOnInit() {
    this.userService.getCurrentUser()
      .then(this.setUser.bind(this))
      .catch(() => {});

    this.userService.getNewNotificationCount()
      .then(count => this.notificationCount = count);
  }

  setUser(user: User) {
    this.isAuthenticated = !!user;
    if (this.isAuthenticated)
      this.username = user.username;
  }

}
