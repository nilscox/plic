import {Component, OnInit} from '@angular/core';
import {User} from '../models/user';
import {UserService} from '../services/user.service';

@Component({
  selector: 'suggest',
  templateUrl: 'client/layout/app.component.html',
  providers: [UserService]
})

export class AppComponent implements OnInit {

  user: User;

  constructor(
    private userService: UserService
  ) {}

  ngOnInit() {
    this.userService.getCurrentUser()
      .then(u => this.user = u)
      .catch(() => {});
  }

}
