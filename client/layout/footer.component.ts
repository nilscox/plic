import {Component} from '@angular/core';

@Component({
  selector: 'footer',
  templateUrl: 'client/layout/footer.component.html',
  styles: [`
.footer a { color: inherit; }
.footer img { max-height: 65px; margin: 4px 10px 0 10px; }
  `]
})

export class FooterComponent {}
