import {Injectable} from '@angular/core';
import {Http} from '@angular/http';

@Injectable()
export class StaticPageService {

  constructor(
    private http: Http
  ) {}

  get(page: string): Promise<string> {
    return this.http.get('/static/' + page + '.html')
      .toPromise()
      .then(r => r.text());
  }

}
