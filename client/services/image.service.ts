import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {HttpRest} from './http-rest';
import {Image_} from '../models/image';
import {Note} from '../models/note';
import {User} from '../models/user';

@Injectable()
export class ImageService {

  rest: HttpRest<Image_>;

  constructor(
    private http: Http
  ) {
    this.rest = new HttpRest<Image_>(http, 'image');
  }

  top(): Promise<Image_[]> {
    return this.rest.request<Image_[]>('get', ['top'], null);
  }

  last(): Promise<Image_[]> {
    return this.rest.request<Image_[]>('get', ['last'], null);
  }

  suggested(): Promise<Image_[]> {
    return this.rest.request<Image_[]>('get', ['suggested'], null);
  }

  addComment(imageId: number, userId: number, comment: string): Promise<Image_> {
    let data = {image: imageId, author: userId, body: comment};
    return this.rest.request<Image_>('post', [imageId, 'comments'], data);
  }

  deleteComment(imageId: number, commentId: number): Promise<Image_> {
    return this.rest.request<Image_>('delete', [imageId, 'comments', commentId]);
  }

  upload(file: File, fields: any): Promise<Image_> {
    return new Promise((resolve, reject) => {
      var formData: any = new FormData();
      formData.append('image', file, file.name);

      Object.keys(fields).forEach(k => formData.append(k, fields[k]));

      var xhr = new XMLHttpRequest();
      xhr.onreadystatechange = () => {
        if (xhr.readyState == 4) {
          if (xhr.status == 201)
            resolve(JSON.parse(xhr.response));
          else
            reject(xhr.response);
        }
      };

      xhr.open('POST', this.rest.mkurl(['upload']), true);
      xhr.send(formData);
    });
  }

}
