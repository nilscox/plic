import {Http} from '@angular/http';

export class HttpRest<T> {

  protected http: Http;
  private urlPrefix: string;

  constructor(http: Http, entityName: string) {
    this.http = http;
    this.urlPrefix = '/api/' + entityName;
  }

  mkurl(path?: any[], opts?: Object) {
    let res = this.urlPrefix + (path ? '/' + path.join('/') : '');
    if (opts)
      res += '?' + Object.keys(opts).map(k => k + '=' + opts[k]).join('&');
    return res;
  }

  request<T>(method: string, args?: any[], data?: Object, opts?: Object): Promise<T> {
    return this.http[method](this.mkurl(args, opts), data)
      .toPromise()
      .then(r => r.json() as T);
  }

  create(data: T, opts?: Object): Promise<T> {
    return this.request<T>('post', null, data, opts);
  }

  read(opts?: Object): Promise<T[]> {
    return this.request('get', null, null, opts);
  }

  readOne(id: number, opts?: Object): Promise<T> {
    return this.request('get', [id], null, opts);
  }

  update(id: number, data: T, opts?: Object): Promise<T> {
    return this.request('put', [id], data, opts);
  }

  delete(id: number, opts?: Object): Promise<T> {
    return this.request('delete', [id], null, opts);
  }

}
