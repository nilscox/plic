import {Injectable, EventEmitter} from '@angular/core';
import {Http} from '@angular/http';
import {HttpRest} from './http-rest';
import {User} from '../models/user';
import {Image_} from "../models/image";
import {Notification} from "../models/notification";

@Injectable()
export class UserService {

  rest: HttpRest<User>;

  currentUser: User;
  token: string;

  public static onUserChange: EventEmitter<User> = new EventEmitter;
  public static onNotificationSeen: EventEmitter<any> = new EventEmitter;

  constructor(
    private http: Http
  ) {
    this.rest = new HttpRest<User>(http, 'user');
  }

  fetchCurrentUser(): Promise<User> {
    return this.rest.request<User>('get', ['current'])
      .then(user => this.currentUser = user);
  }

  getCurrentUser(): Promise<User> {
    if (null != this.currentUser)
      return Promise.resolve(this.currentUser);
    else
      return this.fetchCurrentUser();
  }

  signup(user: User): Promise<User> {
    return this.rest.request<any>('post', ['signup'], user)
      .then(resp => {
        this.token = resp.token;
        return this.setCurrentUser(resp.user);
      });
  }

  signin(username: string, password: string): Promise<User> {
    let data = {
      username: username,
      password: password
    };

    return this.rest.request<User>('post', ['signin'], data)
      .then((res: any) => {
        this.token = res.token;
        this.setCurrentUser(res.user);
        return this.currentUser;
      });
  }

  private setCurrentUser(user: User): User {
    this.currentUser = user;
    UserService.onUserChange.emit(this.currentUser);
    return this.currentUser;
  }

  signout(): Promise<void> {
    // code smell...
    return this.http.get('/api/user/signout').toPromise()
      .then(this.setCurrentUser.bind(this, null));
  }

  updatePassword(id: number, currentPassword: string, newPassword: string) {
    return this.rest.request<User>('put', ['updatepass'], {
      id: id,
      currPassword: currentPassword,
      newPassword: newPassword
    });
  }

  likes(): Promise<Image_> {
    return this.rest.request<Image_>('get', ['likes']);
  }

  addLike(imageid: number): Promise<Image_> {
    return this.rest.request<User>('post', ['like', imageid]);
  }

  removeLike(imageid: number): Promise<Image_> {
    return this.rest.request<User>('delete', ['like', imageid]);
  }

  favorites(): Promise<Image_> {
    return this.rest.request<Image_>('get', ['favorites']);
  }

  addFav(imageid: number): Promise<Image_> {
    return this.rest.request<User>('post', ['fav', imageid]);
  }

  removeFav(imageid: number): Promise<Image_> {
    return this.rest.request<User>('delete', ['fav', imageid]);
  }

  getNotifications(): Promise<Notification[]> {
    return this.rest.request<Notification[]>('get', ['notifications']);
  }

  getNewNotificationCount(): Promise<number> {
    return this.rest.request<any>('get', ['newNotificationCount'])
      .then(json => json.count);
  }

  setNotificationSeen() {
    UserService.onNotificationSeen.emit(null);
  }
}
