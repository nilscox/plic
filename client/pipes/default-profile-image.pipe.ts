import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'defaultProfileImage'
})

export class DefaultProfileImagePipe implements PipeTransform {
  transform(value: any, args: any[]): any {
    if (!value)
      return '/images/default.jpg';
    return value['thumb'];
  }
}
