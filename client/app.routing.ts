import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {TopComponent} from './pages/main/top.component';
import {LastComponent} from './pages/main/last.component';
import {SuggestedComponent} from './pages/main/suggested.component';
import {StaticPageComponent} from './pages/static/static-page.component';
import {SignupComponent} from './pages/auth/signup.component';
import {SigninComponent} from './pages/auth/signin.component';
import {SignoutComponent} from './pages/auth/signout.component';
import {UserProfileComponent} from './pages/user/profile.component';
import {UserEditComponent} from './pages/user/edit.component';
import {NotificationsComponent} from './pages/user/notifications.component';
import {ImageUploadComponent} from './pages/user/image-upload.component';
import {ImageComponent} from './pages/main/image.component';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/top',
    pathMatch: 'full'
  }, {
    path: 'top',
    component: TopComponent
  }, {
    path: 'last',
    component: LastComponent
  }, {
    path: 'suggested',
    component: SuggestedComponent
  }, {
    path: 'legal',
    component: StaticPageComponent
  }, {
    path: 'site-map',
    component: StaticPageComponent
  }, {
    path: 'faq',
    component: StaticPageComponent
  }, {
    path: 'signup',
    component: SignupComponent
  }, {
    path: 'signin',
    component: SigninComponent
  }, {
    path: 'signout',
    component: SignoutComponent
  }, {
    path: 'profile/:username',
    component: UserProfileComponent
  }, {
    path: 'profile/:username/edit',
    component: UserEditComponent
  }, {
    path: 'notifications',
    component: NotificationsComponent
  }, {
    path: 'upload',
    component: ImageUploadComponent
  }, {
    path: 'image/:imageid',
    component: ImageComponent
  }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
