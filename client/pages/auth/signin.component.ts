import {Component} from "@angular/core";
import {Router} from "@angular/router";
import {UserService} from "../../services/user.service";

@Component({
  templateUrl: 'client/pages/auth/signin.component.html',
  providers: [UserService],
  styles: [`
.wrapper { max-width: 640px; margin: auto; }
.links a { color: #666; text-transform: uppercase; font-size: 0.8em; font-weight: bold; cursor: pointer }
form { width: 100%; }
`]
})

export class SigninComponent {

  username: string;
  password: string;

  invalidCredentials: boolean = false;

  constructor(
    private router: Router,
    private userService: UserService
  ) {}

  signin() {
    this.userService.signin(this.username, this.password)
      .then(user => this.router.navigate(['/profile', user.username]))
      .catch(err => {
        if (err.status == 403)
          this.invalidCredentials = true;
        else
          console.log(err);
      });
  }

}
