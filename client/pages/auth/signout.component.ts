import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../../services/user.service';

@Component({
  template: 'Please wait...',
  providers: [UserService]
})

export class SignoutComponent implements OnInit {
    constructor(
      private router: Router,
      private userService: UserService
    ) {}

    ngOnInit() {
      this.userService.signout()
        .then(() => this.router.navigate(['/']))
        .catch(console.error.bind(console));
    }

}
