import {Component} from "@angular/core";
import {Router} from "@angular/router";
import {UserService} from "../../services/user.service";
import {User} from "../../models/user";

@Component({
  templateUrl: 'client/pages/auth/signup.component.html',
  providers: [UserService],
  styles: ['form { max-width: 640px; margin: auto; }']
})

export class SignupComponent {

  user: User = {};
  password: string;

  constructor(
    private router: Router,
    private userService: UserService
  ) {}

  signup() {
    this.user['password'] = this.password;
    this.userService.signup(this.user)
      .then(user => this.router.navigate(['/profile', user.username]))
      .catch(console.log.bind(console));
  }

}
