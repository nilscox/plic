import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {StaticPageService} from '../../services/static-page.service';

declare let require: any;

@Component({
  template: '<div [innerHTML]="content"></div>',
  providers: [StaticPageService]
})

export class StaticPageComponent implements OnInit {

  content: string;

  constructor(
    private route: ActivatedRoute,
    private staticPageService: StaticPageService
  ) {}

  ngOnInit() {
    this.staticPageService.get(this.route.routeConfig.path)
      .then(html => this.content = html);
  }

}
