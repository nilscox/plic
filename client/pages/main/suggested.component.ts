import {Component, OnInit} from '@angular/core';
import {ImageService} from '../../services/image.service';
import {MainComponent} from './main';

@Component({
  templateUrl: 'client/pages/main/main.html',
  styleUrls: ['client/pages/main/main.css'],
  providers: [ImageService]
})

export class SuggestedComponent extends MainComponent implements OnInit {

  title: string = 'Suggested images';

  constructor(
    private imageService: ImageService
  ) { super(); }

  ngOnInit() {
    this.imageService.suggested()
      .then(images => this.images = images)
      .catch(console.error.bind(console));
  }

}
