import {Image_} from '../../models/image';

export class MainComponent {

  images: Image_[];
  carouselImage: Image_;

  getCarouselImages() {
    if (!this.images)
      return [];
    return this.images.slice(0, 4);
  }

  getContentImages() {
    if (!this.images)
      return [];
    return this.images.slice(4);
  }

  onCarouselChanged(image: Image_) {
    this.carouselImage = image;
  }

}
