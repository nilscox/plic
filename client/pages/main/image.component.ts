import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {Image_} from "../../models/image";
import {ImageService} from "../../services/image.service";
import {UserService} from "../../services/user.service";
import {User} from "../../models/user";
import {Comment} from "../../models/comment";

@Component({
  templateUrl: 'client/pages/main/image.component.html',
  styleUrls: ['client/pages/main/image.component.css'],
  providers: [ImageService, UserService]
})

export class ImageComponent implements OnInit {

  image: Image_;
  user: User;
  comment: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private imageService: ImageService,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.userService.getCurrentUser()
      .then(user => this.user = user)
      .catch(console.error.bind(console));

    this.route.params.forEach(param => {
      let id = param['imageid'];
      if (id) {
        this.imageService.rest.readOne(id)
          .then(this.setImage.bind(this))
          .catch(console.error.bind(console));
      }
    });
  }

  setImage(image: Image_) {
    this.image = image;
    this.image.comments.forEach(comment => {
      if (typeof comment.author === 'number')
        this.userService.rest.readOne(<number> comment.author)
          .then(user => comment.author = user)
          .catch(console.error.bind(console));
    });
  }

  postComment() {
    if (this.user === undefined)
      return this.router.navigate(['/signin']);

    this.imageService.addComment(this.image.id, this.user.id, this.comment)
      .then(image => {
        this.image.comments = image.comments;
        this.image.comments.forEach(comment => {
          if (comment.author === this.user.id)
            comment.author = this.user;
        });
        this.comment = '';
      })
      .catch(console.error.bind(console));
  }

  deleteComment(comment: Comment) {

    // DELETE /api/image/:id/comments/:fk
    // This fails. Thanks SailsJS!

    this.imageService.deleteComment(<number> comment.image, comment.id)
      .then(image => this.image = image)
      .catch(() => alert('Expected error.... Aborting.'));
  }

}
