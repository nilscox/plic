import {Component, OnInit} from "@angular/core";
import {UserService} from "../../services/user.service";
import {Notification} from "../../models/notification";

@Component({
  templateUrl: 'client/pages/user/notifications.component.html'
})

export class NotificationsComponent implements OnInit {

  notifications: Array<Notification> = [];

  constructor(
    private userService: UserService
  ) {}

  ngOnInit() {
    this.userService.getNotifications()
      .then(notifications => {
        this.notifications = notifications;
        this.userService.setNotificationSeen();
      });
  }

}
