import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {User} from '../../models/user';
import {UserService} from '../../services/user.service';

@Component({
  templateUrl: 'client/pages/user/profile.component.html',
  styleUrls: ['client/pages/main/main.css'],
  styles: [`
img.profile-image {
  width: 100%;
  margin-top: 20px;
}
`],
  providers: [UserService]
})

export class UserProfileComponent implements OnInit {

  user: User;
  isCurrent: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.route.params.forEach(param => {
      let username = param['username'];
      if (username) {
        this.userService.rest.read({ username: username })
          .then(users => {
            if (users.length > 0)
              this.user = users[0];
            this.userService.getCurrentUser().then(u => this.isCurrent = u.id == this.user.id);
          })
          .catch(console.error.bind(console));
        return;
      }
    });
  }

}
