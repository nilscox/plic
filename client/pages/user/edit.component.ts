import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from '../../models/user';
import {Image_} from '../../models/image';
import {UserService} from '../../services/user.service';

@Component({
  templateUrl: 'client/pages/user/edit.component.html',
  styles: [`
.profile-images img {
  cursor: pointer;
  height: 90px;
}

.profile-images img:hover {
  opacity: 0.8;
}
  `],
  providers: [UserService]
})

export class UserEditComponent implements OnInit {

  currentPassword: string;
  newPassword: string;
  newPasswordConfirm: string;

  user: User;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.activatedRoute.params.forEach(param => {
      let username = param['username'];
      if (username) {
        this.userService.getCurrentUser()
          .then(user => {

            if (user.username != username)
              this.router.navigate(['/profile', user.username, 'edit']);

            // Fetch images
            return this.userService.rest.readOne(user.id);
          }).then(user => this.user = user);
      }
    });
  }

  editPassword() {
    if (this.newPassword !== this.newPasswordConfirm)
        return;

    this.userService.updatePassword(this.user.id, this.currentPassword, this.newPassword)
      .then(user => this.router.navigate(['/profile', user.username]))
      .catch(console.error.bind(console));
  }

  saveProfileInfo() {
    this.userService.rest.update(this.user.id, this.user)
      .then(user => this.router.navigate(['/profile', user.username]))
      .catch(console.error.bind(console));
  }

  setProfileImage(image: Image_) {
    this.user.profileImage = image.id;
    this.saveProfileInfo();
  }

}
