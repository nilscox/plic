import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {User} from '../../models/user';
import {UserService} from '../../services/user.service';
import {ImageService} from '../../services/image.service';

@Component({
  templateUrl: 'client/pages/user/image-upload.component.html',
  providers: [UserService, ImageService],
})

export class ImageUploadComponent implements OnInit {

  user: User;

  file: File;

  title: string;

  constructor(
    private router: Router,
    private userService: UserService,
    private imageService: ImageService
  ) {}

  ngOnInit() {
    this.userService.getCurrentUser()
      .then(user => {
        if (!user)
          this.router.navigate(['/signin']);
        this.user = user;
      }).catch(console.error.bind(console));
  }

  fileChangeEvent(event: any) {
    if (event.target.files.length > 0)
      this.file = event.target.files[0];
  }

  uploadImage() {
    this.imageService.upload(this.file, { 'title': this.title })
      .then(image => this.router.navigate(['/image', image.id]))
      .catch(console.error.bind(console));
  }

}
