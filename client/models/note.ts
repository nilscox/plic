import {Image_} from './image';
import {User} from './user';

export interface Note {
  id?: number,
  note?: number
  user?: number | User;
  image?: number | Image_;
  createdAt?: string;
  updatedAt?: string;
}
