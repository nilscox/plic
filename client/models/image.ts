import {User} from './user';
import {Comment} from './comment';

export interface Image_ {
  id?: number;
  url?: string;
  title?: string;
  owner?: number | User;
  comments?: Comment[];
  createdAt?: string;
  updatedAt?: string;
  likedBy: User[];
  favoritedBy: User[];
}
