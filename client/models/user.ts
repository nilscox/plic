import {Image_} from './image';

export interface User {
  id?: number;
  username?: string;
  email?: string;
  firstname?: string;
  lastname?: string;
  summary?: string;
  profileImage?: number | Image_;
  favorites?: Image_[];
  likes?: Image_[];
  images?: Image_[];
}
