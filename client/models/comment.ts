import {Image_} from './image';
import {User} from './user';

export interface Comment {
  id?: number;
  image?: number | Image_;
  author?: number | User;
  body?: string;
  createdAt?: string;
  updatedAt?: string;
}
