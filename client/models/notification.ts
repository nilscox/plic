import {Image_} from './image';
import {User} from './user';
import {Comment} from './comment';

export interface Notification {
  id?: number;
  type?: string;
  owner?: number | User;
  image?: number | Image_;
  user?: number | User;
  comment?: number | Comment;
  seen?: boolean;
  createdAt?: string;
  updatedAt?: string;
}
