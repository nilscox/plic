import {Component, Input} from '@angular/core';
import {Image_} from '../models/image';

@Component({
  selector: 'image-display',
  templateUrl: 'client/components/image-display.component.html',
  styles: [`
img.thumb { height: 130px; }
img.modal-image { display: block; }
`]
})

export class ImageDisplayComponent {

  @Input()
  image: Image_;

}
