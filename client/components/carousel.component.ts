import {Component, Input, Output, OnChanges, EventEmitter} from '@angular/core';
import {Image_} from '../models/image';
import {UserService} from '../services/user.service';
import {ImageService} from '../services/image.service';

@Component({
  selector: 'carousel',
  providers: [UserService, ImageService],
  templateUrl: 'client/components/carousel.component.html',
  styles: [`
.carousel {
  background: black;
}

.carousel img {
  max-width: 100%;
  max-height: 380px;
  display: none;
  margin: auto;
}

.carousel img.active {
  display: block;
}
  `]
})

export class CarouselComponent implements OnChanges {

  @Input()
  images: Image_[];

  @Input()
  interval: number = 5000;

  @Output()
  onImageChanged: EventEmitter<Image_> = new EventEmitter<Image_>();

  currentImage: number = null;

  ngOnChanges() {
    if (null == this.currentImage && this.images && this.images.length) {
      this.currentImage = 0;
      this.onImageChanged.emit(this.images[this.currentImage]);
      setInterval(this.nextImage.bind(this), this.interval);
    }
  }

  nextImage() {
    if (null != this.currentImage) {
      this.currentImage++;
      this.currentImage %= this.images.length;
      this.onImageChanged.emit(this.images[this.currentImage]);
    }
  }

}
