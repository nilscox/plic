import {Component, Input} from '@angular/core';
import {Notification} from '../models/notification';

@Component({
  selector: 'notification',
  templateUrl: 'client/components/notification.component.html',
  styles: [`
.date {
  text-align: right;
  font-size: 0.8em;
  color: #333;
}

.icon {
  vertical-align: text-top;
  font-size: 1.2em;
  padding: 0 10px;
}

.icon-favorite {
  color: #d58512;
}

.icon-like {
  color: #AA3333;
}

.icon-comment {
  color: #666666;
}

.new {
  color: #006600;
}
`]
})

export class NotificationComponent {

  @Input()
  notification: Notification;

}
