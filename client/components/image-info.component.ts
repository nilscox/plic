import {Component, Input, OnChanges} from "@angular/core";
import {Image_} from "../models/image";
import {ImageService} from "../services/image.service";
import {UserService} from "../services/user.service";
import {Comment} from '../models/comment';

@Component({
  selector: 'image-info',
  providers: [ImageService, UserService],
  templateUrl: 'client/components/image-info.component.html',
  styles: [`
.image-info {
  max-height: 100%;
}

.image-title {
  width: 100%;
  text-align: center;
  font-weight: bold;
  margin: 15px 0;
}

.image-creationdate {
  font-size: 0.8em;
  font-weight: bold;
  text-align: right;
  color: #666666;
}

.comments a {
  font-weight: bold;
}
  `]
})

export class ImageInfoComponent implements OnChanges {

  @Input()
  image: Image_;

  comment: string;

  constructor(
    private imageService: ImageService,
    private userService: UserService
  ) {}

  ngOnChanges() {
    if (this.image && this.image.comments === undefined) {
      this.imageService.rest.request<Comment[]>('get', [this.image.id, 'comments'])
        .then(this.setImageComments.bind(this))
        .catch(console.error.bind(console));
    }
  }

  setImageComments(comments: Comment[]) {
    this.image.comments = comments.map(comment => {
      this.userService.rest.readOne(<number> comment.author, { populate: 'profileImage' })
        .then(user => {
          return comment.author = user
        });
      return comment;
    });
  }

}
