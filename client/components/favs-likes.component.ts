import {Component, OnInit, Input} from "@angular/core";
import {Router} from "@angular/router";
import {Image_} from "../models/image";
import {User} from "../models/user";
import {UserService} from "../services/user.service";

@Component({
  selector: 'favs-likes',
  providers: [UserService],
  templateUrl: 'client/components/favs-likes.component.html',
  styles: [`
.favs-likes {
  font-weight: bold;
  font-size: 1.1em;
}

.favs-likes .icon {
  font-size: 1.2em;
  vertical-align: top;
  cursor: pointer;
}

.favs-likes .fav {
  color: #d58512;
}

.favs-likes .like {
  color: #AA3333;
}

.favs-list, .likes-list {
  position: absolute;
  top: 100%;
  left: 0;
  z-index: 10;
  background: rgba(0, 0, 0, .8);
  color: whitesmoke;
  padding: 5px 15px;
  border-radius: 3px;
}

.favs-list a, .likes-list a {
  color: inherit;
}
`]
})

export class FavsLikesComponent implements OnInit {

  @Input()
  image: Image_;

  @Input()
  inline: boolean = false;

  user: User;

  showFavsList: boolean = false;
  showLikesList: boolean = false;

  constructor(
    private router: Router,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.userService.getCurrentUser()
      .then(user => this.user = user)
      .catch(console.error.bind(console));
  }

  checkLogin() {
    if (this.user === undefined) {
      this.router.navigate(['/signin']);
      return true;
    }

    return false;
  }

  like() {
    if (this.checkLogin()) return;
    this.userService.addLike(this.image.id)
      .then(image => this.image.likedBy.unshift(this.user))
      .catch(console.error.bind(console));
  }

  unlike() {
    if (this.checkLogin()) return;
    this.userService.removeLike(this.image.id)
      .then(image => this.image.likedBy.splice(this.image.likedBy.indexOf(this.user), 1))
      .catch(console.error.bind(console));
  }

  favorite() {
    if (this.checkLogin()) return;
    this.userService.addFav(this.image.id)
      .then(image => this.image.favoritedBy.unshift(this.user))
      .catch(console.error.bind(console));
  }

  unfavorite() {
    if (this.checkLogin()) return;
    this.userService.removeFav(this.image.id)
      .then(image => this.image.favoritedBy.splice(this.image.favoritedBy.indexOf(this.user), 1))
      .catch(console.error.bind(console));
  }

  isUserLike() {
    if (this.user === undefined)
      return false;
    return undefined !== this.image.likedBy.find(u => u.id == this.user.id);
  }

  isUserFav() {
    if (this.user === undefined)
      return false;
    return undefined !== this.image.favoritedBy.find(u => u.id == this.user.id);
  }

}
