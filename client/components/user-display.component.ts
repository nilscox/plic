import {Component, Input} from '@angular/core';
import {User} from '../models/user';

@Component({
  selector: 'user-display',
  templateUrl: 'client/components/user-display.component.html',
  styleUrls: ['client/components/user-display.component.css']
})

export class UserDisplayComponent {

  @Input()
  user: User;

  @Input()
  showProfileLink: boolean = true;

}
