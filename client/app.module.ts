import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpModule} from '@angular/http';
import {FormsModule} from '@angular/forms';

import {routing} from './app.routing';

import {AppComponent} from './layout/app.component';
import {FooterComponent} from './layout/footer.component';
import {NavbarComponent} from './layout/navbar.component';

import {StaticPageComponent} from './pages/static/static-page.component';

import {CarouselComponent} from './components/carousel.component';
import {ImageDisplayComponent} from './components/image-display.component';
import {ImageInfoComponent} from './components/image-info.component';
import {UserDisplayComponent} from './components/user-display.component';
import {FavsLikesComponent} from './components/favs-likes.component';

import {TopComponent} from './pages/main/top.component';
import {LastComponent} from './pages/main/last.component';
import {SuggestedComponent} from './pages/main/suggested.component';

import {SignupComponent} from './pages/auth/signup.component';
import {SigninComponent} from './pages/auth/signin.component';
import {SignoutComponent} from './pages/auth/signout.component';

import {UserProfileComponent} from './pages/user/profile.component';
import {UserEditComponent} from './pages/user/edit.component';
import {ImageUploadComponent} from './pages/user/image-upload.component';
import {NotificationsComponent} from './pages/user/notifications.component';
import {NotificationComponent} from './components/notification.component';

import {ImageComponent} from './pages/main/image.component';

import {DefaultProfileImagePipe} from './pipes/default-profile-image.pipe';

import 'rxjs/add/operator/toPromise';

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    routing
  ],
  declarations: [
    AppComponent,
    FooterComponent,
    NavbarComponent,

    StaticPageComponent,

    CarouselComponent,
    ImageDisplayComponent,
    ImageInfoComponent,
    UserDisplayComponent,
    FavsLikesComponent,

    TopComponent,
    LastComponent,
    SuggestedComponent,

    SignupComponent,
    SigninComponent,
    SignoutComponent,

    UserProfileComponent,
    UserEditComponent,
    ImageUploadComponent,
    NotificationsComponent,
    NotificationComponent,

    ImageComponent,

    DefaultProfileImagePipe,
  ],
  bootstrap: [AppComponent]
})

export class AppModule {}
