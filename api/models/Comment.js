/**
 * Comment.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    image: {
      model: 'Image',
      required: true
    },

    author: {
      model: 'User',
      required: true
    },

    body: {
      type: 'text'
    }

  },

  afterCreate: function(values, cb) {
    Image.findOne({ id: values.image })
      .then(image => {
        return Notification.create({
          type: 'comment',
          owner: image.owner,
          image: image,
          comment: values,
          user: values.author
        })
      })
      .then(() => cb())
      .catch(cb);
  }

}

;

