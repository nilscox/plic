/**
 * Image.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

let path = require('path');

module.exports = {

  attributes: {

    filename: {
      type: 'string',
      required: true,
      notNull: true
    },

    title: {
      type: 'string',
    },

    owner: {
      model: 'user'
    },

    likedBy: {
      collection: 'user',
      via: 'likes'
    },

    favoritedBy: {
      collection: 'user',
      via: 'favorites'
    },

    comments: {
      collection: 'comment',
      via: 'image'
    },

    toJSON: function() {
      let obj = this.toObject();
      obj.url = ['hd', 'web', 'thumb'].reduce((o, k) => {
        o[k] = '/' + path.join('images', k, path.basename(this.filename));
        return o;
      }, {});
      return obj;
    }

  }

};
