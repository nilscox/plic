/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    username: {
      type: 'string',
      required: true,
      unique: true
    },

    email: {
      type: 'string',
      required: true,
      email: true,
      unique: true
    },

    password: {
      type: 'string',
      required: true
    },

    firstname: {
      type: 'string'
    },

    lastname: {
      type: 'string'
    },

    summary: {
      type: 'string'
    },

    profileImage: {
      model: 'image'
    },

    likes: {
      collection: 'image',
      via: 'likedBy'
    },

    favorites: {
      collection: 'image',
      via: 'favoritedBy'
    },

    images: {
      collection: 'image',
      via: 'owner'
    },

    toJSON: function() {
      let obj = this.toObject();
      delete obj.password;
      return obj;
    }

  }

};
