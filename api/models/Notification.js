/**
 * Notification.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    owner: {
      model: 'user',
      required: true,
      notNull: true
    },

    type: {
      type: 'string',
      required: true,
      notNull: true
    },

    user: {
      model: 'user'
    },

    image: {
      model: 'image'
    },

    comment: {
      model: 'comment'
    },

    seen: {
      type: 'boolean',
      required: true,
      notNull: true,
      defaultsTo: false
    },

  },

};

