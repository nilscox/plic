/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

let bcrypt = require('bcrypt');
let jwt = require('jsonwebtoken');

const SALT_ROUNDS = 10;

let likefav = (rel, action) =>
  (req, res) => {
    if (!req.session.userid)
      return res.forbidden();

    let image = null;
    let user = null;

    Image.findOne({ id: parseInt(req.param('id')) })
      .populate(rel + 'dBy')
      .populate('owner')
      .then(i => {
        if (!i)
          return res.notFound();

        image = i;
        return User.findOne({ id: req.session.userid });
      })
      .then(u => {
        if (!u)
          return res.notFound();

        user = u;
        user[rel + 's'][action](image.id);

        return user.save();
      })
      .then(err => {
        if (err) throw err;
        image[rel + 'dBy'][action](user.id);
        return image.save();
      })
      .then(err => {
        if (err) throw err;

        if (user.id === image.owner || action !== 'add')
          return null;

        return Notification.create({
          type: rel,
          owner: image.owner,
          user: user,
          image: image
        });
      })
      .then(() => res.json(image))
      .catch(res.serverError.bind(res));
  };

module.exports = {

	signup: (req, res) => {
		let username = req.body.username;
    let password = req.body.password;

    if (!username || !password)
        return res.badRequest();

		bcrypt.hash(password, SALT_ROUNDS, (err, hash) => {
			if (err)
				return res.serverError();

			let user = req.body;
			user.password = hash;

			User.create(user).then(user => {
				if (!user)
					return res.serverError();

        let token = jwt.sign({ username: user.username }, sails.config.jwt.secret);
        req.session.userid = user.id;

				return res.created({
          token: token,
          user: user,
        });
			})
        .catch(res.badRequest.bind(res));
		});
	},

	signin: (req, res) => {
    let username = req.body.username;
    let password = req.body.password;

    if (!username || !password)
        return res.badRequest();

    User.findOne({ username: username }).then(user => {
        if (!user)
            return res.forbidden();

				bcrypt.compare(password, user.password, (err, result) => {
					if (err)
						return res.serverError();

					if (!result)
						return res.forbidden();

					let token = jwt.sign({ username: user.username }, sails.config.jwt.secret);
					req.session.userid = user.id;

					return res.json({
						token: token,
						user: user
					});
				});

    });
	},

	signout: (req, res) => {
		if (!req.session.userid)
			return res.badRequest();

		delete req.session.userid;
		return res.end();
	},

	current: (req, res) => {
		if (!req.session.userid)
			return res.notFound();

		User.findOne({ id: req.session.userid })
      .populate('profileImage')
      .then(user => {
			if (!user)
				return res.notFound();

			res.json(user);
		}).catch(res.badRequest.bind(res))
	},

  updatepass: (req, res) => {
    let id = req.body.id;
    let currPassword = req.body.currPassword;
    let newPassword = req.body.newPassword;

    if (!username || !currPassword || !newPassword)
      return res.badRequest();

    User.findOne(id).then(user => {
      if (!user)
        return res.forbidden();

      bcrypt.compare(currPassword, user.password, (err, result) => {
        if (err)
          return res.serverError();

        if (!result)
          return res.forbidden();

        bcrypt.hash(newPassword, SALT_ROUNDS, (err, hash) => {
          if (err)
            return res.serverError();

          User.update({password: hash}).then(user => {
            if (!user)
              return res.serverError();
            return res.json(user);
          });

        });

      });
    }).catch(res.badRequest.bind(res));
  },

  notifications: (req, res, next) => {
    if (!req.session.userid)
      return next();

    let notifications = null;

    Notification
      .find({
        owner: req.session.userid
      })
      .populate('user')
      .populate('image')
      .populate('comment')
      .sort('createdAt DESC')
      .then(notifs => {
        notifications = notifs;
        return Notification.update({ seen: false }, { seen: true });
      })
      .then(() => res.json(notifications))
      .catch(res.serverError.bind(res));
  },

  newnotificationCount: (req, res, next) => {
    if (!req.session.userid)
      return next();

    Notification
      .count({
        where: {
          owner: req.session.userid,
          seen: false
        }
      })
      .then(count => res.json({count: count}))
      .catch(res.serverError.bind(res));
  },

  like: likefav('like', 'add'),
  unlike: likefav('like', 'remove'),
  fav: likefav('favorite', 'add'),
  unfav: likefav('favorite', 'remove'),

};
