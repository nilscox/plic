/**
 * ImageController
 *
 * @description :: Server-side logic for managing images
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

let path = require('path');
let sharp = require('sharp');

module.exports = {

  top: (req, res) => {
    Image.find()
      .populate('owner')
      .populate('likedBy')
      .populate('favoritedBy')
      .then(images => {
        Promise.all(images.map(image =>
          sails.models.image.findOne({ id: image.owner.profileImage })
        )).then(pimgs => {
          res.json(images.map((img, i) => { img.owner.profileImage = pimgs[i]; return img }));
        }).catch(res.serverError.bind(res));
      })
      .catch(res.serverError.bind(res));
  },

  last: (req, res) => {
    Image.find()
      .populate('owner')
      .populate('likedBy')
      .populate('favoritedBy')
      .sort('createdAt DESC')
      .then(images => {
        Promise.all(images.map(image =>
          sails.models.image.findOne({ id: image.owner.profileImage })
        )).then(pimgs => {
          res.json(images.map((img, i) => { img.owner.profileImage = pimgs[i]; return img }));
        }).catch(res.serverError.bind(res));
      })
      .catch(res.serverError.bind(res));
  },

  suggested: (req, res) => {
    Image.find()
      .populate('owner')
      .populate('likedBy')
      .populate('favoritedBy')
      .sort('updatedAt')
      .then(images => {
        Promise.all(images.map(image =>
          sails.models.image.findOne({ id: image.owner.profileImage })
        )).then(pimgs => {
          res.json(images.map((img, i) => { img.owner.profileImage = pimgs[i]; return img }));
        }).catch(res.serverError.bind(res));
      })
      .catch(res.serverError.bind(res));
  },

  upload: (req, res) => {
    if (!req.session.userid)
      return res.forbidden();

    req.file('image').upload({
      maxBytes: 6000000, // ~6 MB
      dirname: path.resolve(sails.config.appPath, 'uploads'),
    }, (err, uploadedFiles) => {
      if (err)
        return res.serverError(err);

      if (uploadedFiles.length !== 1)
        return res.badRequest();

      let filepath = uploadedFiles[0].fd;
      let filename = path.basename(filepath);
      let medatada = {};

      sharp(filepath)
        .metadata()
        .then(data => {
          medatada = data;
          return sharp(filepath)
            .resize(Math.min(medatada.width, sails.config.image.hd.size))
            .toFile(path.resolve(sails.config.appPath, sails.config.image.hd.path, filename));
        })
        .then(() => {
          return sharp(filepath)
            .resize(Math.min(medatada.width, sails.config.image.web.size))
            .toFile(path.resolve(sails.config.appPath, sails.config.image.web.path, filename));
        })
        .then(() => {
          return sharp(filepath)
            .resize(Math.min(medatada.width, sails.config.image.thumb.size))
            .toFile(path.resolve(sails.config.appPath, sails.config.image.thumb.path, filename));
        })
        .then(() => {

          let image = req.body;
          image.filename = path.basename(uploadedFiles[0].fd);
          image.owner = req.session.userid;

          Image.create(image).then(image => {
            if (!image)
              return res.serverError();
            return res.created(image);
          });

        })
        .catch(res.serverError.bind(res));

    });
  },

};

